//
//  NSDate+//ODSDASSERTate.h
//  ODSCommon
//
//  Created by Heiko Maaß on 06.09.11.
//  Copyright (c) 2011 online directory service gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (ODSDate)

/*
 Gibt die aktuelle Zeit in ms seit 1970 zurück
 */
+(long long) timestampInMs;
@end

