//
//  GSAdServerRequestFactory.m
//  ODSCommon
//
//  Created by Sandra Majewski on 09.10.13.
//  Copyright (c) 2013 online directory service gmbh. All rights reserved.
//

#import "GSAdServerRequestFactory.h"
#import "UIDevice+ODSDevice.h"
#import "NSString+GSStringUtils.h"
#import "NSString+ODSStringHashFunctions.h"

@implementation GSAdServerRequestFactory
@synthesize urlBase;
@synthesize params;

NSString* const AS_WS_ENCODING								= @"UTF-8";
NSStringEncoding const AS_WS_NSSTRINGENCODING				= NSUTF8StringEncoding;
NSStringEncoding const AS_WS_URL_PARAM_NSSTRINGENCODING		= NSISOLatin1StringEncoding;

- (NSMutableURLRequest *) requestWithValuesForPlaceholder {
    
    /* urlBase hat drei verschiedene Ausprägungen:
     
     Adserver Request Adpepper (nur obligatorische Parameter):
     
     <adserver>
     <url>http://ad.mo.doubleclick.net/dartproxy/mobile.handler? k=N4164/tbm-122/smart_treffer;sz=@size@&;kw=B%C3%B6rse;
     ￼plz=90461;stadt=K%C3%B6ln;buid=5;apv=tb-iphone;apv=3.90&r=x&u=???&forecast=1 <url>
     </adserver>
     ￼
     Adserver Request Addition (nur obligatorische Parameter):
     
     <adserver>
     <url> http://mobile.adfarm1.adition.com/588/banner?sid=12345& prf[sz]=@size@&prf[kw]=B%C3%B6rse&prf[plz]=79111&prf[stadt]=darmstadt&prf[buid]=12345&gps
     ￼￼    <url>
     </adserver>
     
     Adserver Request TENSQUARE (nur obligatorische Parameter):
     
     <adserver>
     <url> http://mobile.ads.10sq.net/get?qb=tb-m-treffer&pn=166& bi=17&kw=B%C3%B6rse&sz=@size@&s=@wipe_s@&apv=tb-iphone&apn=3.90
     ￼    <url>
     </adserver>
     
     */
    
    /* TestURLs der Anbieter
    NSString *testURLAdition = @"http://mobile.adfarm1.adition.com/1/banner?sid=2701502&prf[k]=&prf[sz]=&prf[kw]=autohaus&prf[stadt]=freiburg&prf[plz]=&prf[buid]=&gk=&wpt=x";
    NSString *testURLTensquare = @"http://mobile.ads.tensq.net/get/?qb=gs-m-treffer&sc=gs-mobil&pn=161&bi=32&kw=Mustereintrag&loc=Essen";*/
    
    //Weitere Parameter ergänzen
    
    
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *appVersion = [infoDict objectForKey:@"CFBundleShortVersionString"]; // example: 1.0.0    
    CGRect mainBounds = [[UIScreen mainScreen] bounds];
    NSString *resolution = [NSString stringWithFormat:@"%fx%f", mainBounds.size.width, mainBounds.size.height];
    NSString *gk = @"5"; //Geräteklassen anhand Displaybreite 1 = <128, 2 = <176, 3 = <240, 4 = <320, 5 = >=320
    
    __block NSMutableDictionary *moreParams = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      @"gs-ipad", @"@apn@",
                                      appVersion, @"@apv@",
                                      [[self createUserAgentForRequest] URLEncodedString_de], @"@user_agent@",
                                      @"1", @"@dcaps@",
                                      resolution, @"@display_resolution@",
                                      @"", @"@client_ip@",
                                      gk, @"@device@",
                                      [self wipeUuid], @"@wipe_u@",
                                      nil];
    
    [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [moreParams setObject:obj forKey:key];
    }];
    
    params = nil;
    params = [NSDictionary dictionaryWithDictionary:moreParams];
    
    NSParameterAssert(self.urlBase != nil);
    __block NSString *urlWithReplacedParams = urlBase;
    [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSString *paramValue = (NSString*)obj;
        urlWithReplacedParams = [urlWithReplacedParams stringByReplacingOccurrencesOfString:key withString:paramValue];
    }];


    NSURL *url = [NSURL URLWithString: urlWithReplacedParams];
    
	NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL: url cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval: 30];
	[req setValue: @"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField: @"Accept"];
	[req setValue: AS_WS_ENCODING forHTTPHeaderField: @"Accept-Charset"];
	[req setValue: @"de,en;q=0.8,de-de;q=0.5,en-us;q=0.3" forHTTPHeaderField: @"Accept-Language"];
	[req setValue: @"gzip" forHTTPHeaderField: @"Accept-Encoding"];
	[req setValue:[url host] forHTTPHeaderField: @"Host"];
	[req setValue: @"max-age=0" forHTTPHeaderField: @"Cache-Control"];
    [req setValue: [[self createUserAgentForRequest]  URLEncodedString_de] forHTTPHeaderField:@"user-agent"];
	return req;
}

- (NSString*) createUserAgentForRequest {
   /* Vor jedem Request muss der User-Agent im Header gesetzt werden. Der User-Agent folgt dabei genau definiert Vorgaben.
    iPhone:
         Das Telefonbuch/[Version] (iPhone; U; CPU OS [Version] like Mac OS X; de-de) AppleWebKit DasTelefonbuch/[Build] 
    Bsp: Das Telefonbuch/2.31 (iPhone; U; CPU OS 6.0.1 like Mac OS X; de-de) AppleWebKit DasTelefonbuch/1753
    ￼￼
    iPad:
         Das Telefonbuch/[Version] (iPad; U; CPU OS [Version] like Mac OS X; de-de) AppleWebKit DasTelefonbuch/[Build] 
    Bsp: Das Telefonbuch/2.31 (iPad; U; CPU OS 5.1 like Mac OS X; de-de) AppleWebKit DasTelefonbuch/1753
    */
    NSString *userAgentString = nil;
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *devRegion = [infoDict objectForKey:@"CFBundleDevelopmentRegion"]; // example: de_DE
    NSString *appVersion = [infoDict objectForKey:@"CFBundleShortVersionString"]; // example: 1.0.0
    NSNumber *buildNumber = [infoDict objectForKey:@"DTSDKBuild"]; // example: 42
    NSString *systemName = [UIDevice currentDevice].systemName;
    NSString *systemVersion = [UIDevice currentDevice].systemVersion;
    if ([[UIDevice currentDevice] isIpad]) {
        //Das Telefonbuch/2.31 (iPad; U; CPU OS 5.1 like Mac OS X; de-de) AppleWebKit DasTelefonbuch/1753
        //GelbeSeiten/1.5.2 (iPad; U; CPU OS 6.0.1 like iPhone OS; de_DE) AppleWebKit GelbeSeiten/11
        userAgentString = [NSString stringWithFormat:@"GelbeSeiten/%@ (iPad; U; CPU OS %@ like %@; %@) AppleWebKit GelbeSeiten/%@", appVersion, systemVersion, systemName, devRegion, buildNumber];        
    } else if ([[UIDevice currentDevice] isIpodTouch]) {
        userAgentString = [NSString stringWithFormat:@"GelbeSeiten/%@ (iPod; U; CPU OS %@ like %@; %@) AppleWebKit GelbeSeiten/%@", appVersion, systemVersion, systemName, devRegion, buildNumber];
    } else {
        userAgentString = [NSString stringWithFormat:@"GelbeSeiten/%@ (iPhone; U; CPU OS %@ like %@; %@) AppleWebKit GelbeSeiten/%@", appVersion, systemVersion, systemName, devRegion, buildNumber];
    }
    
    return userAgentString;
}

- (NSString*) wipeUuid {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // User ID
    NSString *userId = (NSString*) [prefs objectForKey:@"WWA_USERINFO_UID"];
    if (userId == nil) {
        userId = [NSString uuid];
    }
    return userId;
}

- (void) checkForRequiredParameters {
    __block NSArray *requiredParams = [NSArray arrayWithObjects:@"@size@", @"@wipe_s", @"@display_resolution@", @"@apn@", @"@apv@", @"@dcaps", nil];
    [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSString *paramKey = (NSString*)key;
        NSString *paramValue = (NSString*)obj;
        if ([requiredParams containsObject: paramKey]) {
             NSParameterAssert(paramValue != nil);
        }
    }];
}

#pragma mark -
#pragma mark lifecycle
- (id) initWithUrl:(NSString*) url andParams:(NSDictionary*) inParams {
	if (self = [super init]) {
        self.urlBase = url;
        self.params = inParams;
	}
	return self;
}


- (void) dealloc {
    urlBase = nil;
    params = nil;
}

@end
