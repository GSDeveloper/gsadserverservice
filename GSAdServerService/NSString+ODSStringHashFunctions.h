//
//  NSString+ODSStringHashFunctions.h
//  ODSCommon
//
//  Created by Heiko Maaß on 08.09.11.
//  Copyright (c) 2011 online directory service gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ODSStringHashFunctions)

+ (NSString*) uuid;

@end

@interface NSODSStringHashFunctionsDummy : NSObject
@end