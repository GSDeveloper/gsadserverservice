//
//  GSAdServerRequest.m
//  ODSCommon
//
//  Created by Sandra Majewski on 09.10.13.
//  Copyright (c) 2013 online directory service gmbh. All rights reserved.
//

#import "GSAdServerRequest.h"
#import "GSAdServerRequestFactory.h"
#import "GSAdServerMobileCreativePO.h"
#import "NSDate+ODSDate.h"

@implementation GSAdServerRequest

@synthesize delegate;

- (void) loadAndParse {
    NSParameterAssert(self.connection != nil);
    
    self.timestampRequestStarted = [NSDate timestampInMs];
    /*ODSQueryBuilder *odsQueryBuilder = [[ODSQueryBuilder alloc] init];
    [odsQueryBuilder addParameter:@"id" value:subscriberId];
    if (![NSString isStringNilOrEmpty:self.trdId]) {
        [odsQueryBuilder addParameter:@"trd_id " value:trdId];
    }
    [odsQueryBuilder addParameter:@"modelType" value:@"iphone"];*/
    NSURLRequest *req = [self.urlRequestFactory requestWithValuesForPlaceholder];
	[self.connection loadRequest: req];
}

- (void) loadAndParseFromLocalFile {
    //NSString * filePath = [[NSBundle mainBundle] pathForResource:@"CreativeMobileImage" ofType:@"xml"];   CreativeMobileCustomHTML
    NSString * filePath = [[NSBundle mainBundle] pathForResource:@"CreativeMobileCustomHTML" ofType:@"xml"]; 
    NSData * fileData = [NSData dataWithContentsOfFile:filePath];
    [self startParsingWithLocalData:fileData];
}

- (void) failedWithError: (NSError *) error {
	[delegate wsDidFailWithError: error request:self];
}

#pragma mark -
#pragma mark delegates

- (void) analyzeResponse: (GSAdServerMobileCreativePO *) creativeMobile {
    self.performanceInMs = [NSDate timestampInMs] - self.timestampRequestStarted;
	
    if (creativeMobile != nil && (creativeMobile.image != nil || creativeMobile.customHtml != nil)) {
        [delegate wsDidReceiveAdvertising:creativeMobile request:self];

    }
	else {
		NSError *error = [NSError errorWithDomain:@"AdServerService" code:110 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"AdServerResponse konnte nicht geparst werden", @"Reason", nil]];
		[delegate wsDidFailWithError: error request:self];
	}
}


#pragma mark -
#pragma mark life cycle
- (void) dealloc {
	[self setDelegate: nil];
}

@end
