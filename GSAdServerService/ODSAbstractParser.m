//
//  ODSAbstractParser.m
//  Favicinity
//
//  Created by Heiko Maaß on 28.05.10.
//  Copyright 2010 ODS Marketing. All rights reserved.
//

#import "ODSAbstractParser.h"

@implementation ODSAbstractParser
@synthesize xmlParser, delegate, testDelegate;

- (void)parseInBackgroundThread {
	[self performSelectorInBackground:@selector(parse) withObject:nil];
}

- (void)parse {
	@autoreleasepool {
		[xmlParser setDelegate:self];
		[xmlParser parse];
	}
}

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    //
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError {
    NSLog(@"Parser Validation Error occurred: %@", [validationError description]);
}

- (void)cancelParsing {
	[xmlParser abortParsing];
}

#pragma mark -
#pragma mark xml parser delegate

- (void)parser:(NSXMLParser*)parser didEndElement:(NSString*)elem namespaceURI:(NSString*)namespace qualifiedName:(NSString*)qName {
    //NSLog(@"Parser did end Element: %@ with namespace: %@ and qualified name: %@", elem, namespace, qName);
}

- (void)parserDidEndDocument:(NSXMLParser*)parser {
	[parser setDelegate:nil];
	[delegate performSelectorOnMainThread:@selector(parserDidParse) withObject:nil waitUntilDone:NO];
	[testDelegate parserDidParse]; //for unit tests only
}

- (void)parser:(NSXMLParser*)parser parseErrorOccurred:(NSError*)error {
	[parser setDelegate:nil];
	[delegate performSelectorOnMainThread:@selector(parserDidFailWithError:) withObject:error waitUntilDone:NO];
	[testDelegate parserDidFailWithError:error]; //for unit tests only
}

#pragma mark -
#pragma mark life cycle
- (id)initWithData:(NSData*)data {
	if(self = [super init]) {
		NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
		[self setXmlParser:parser];
	}
	return self;
}

- (void) dealloc {
	[self setDelegate:nil];	
	[self setTestDelegate:nil];
}


@end
