//
//  NSDate+//ODSDASSERTate.m
//  ODSCommon
//
//  Created by Heiko Maaß on 06.09.11.
//  Copyright (c) 2011 online directory service gmbh. All rights reserved.
//

#import "NSDate+ODSDate.h"

@implementation NSDate (ODSDate)

+ (long long) timestampInMs {
    double now = [[NSDate date] timeIntervalSince1970] * 1000;
    return [[NSNumber numberWithDouble:now] longLongValue];
}
@end
