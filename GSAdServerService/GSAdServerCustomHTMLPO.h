//
//  GSAdServerCustomHTMLPO.h
//  ODSCommon
//
//  Created by Sandra Majewski on 23.10.13.
//  Copyright (c) 2013 online directory service gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ODSElement.h"

@interface GSAdServerCustomHTMLPO : ODSElement

@property (nonatomic, strong) NSString *customHTMLString;

@end
