//
//  UIDevice+//ODSDASSERTevice.h
//  Favicinity
//
//  Created by Heiko Maaß on 28.05.10.
//  Copyright 2010 ODS Marketing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIDevice (ODSDevice)
-(BOOL) isIpodTouch;
-(BOOL) isIpad;
@end

//http://iphonedevelopmentexperiences.blogspot.com/2010/03/categories-in-static-library.html




