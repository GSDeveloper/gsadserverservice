//
//  UIDevice+//ODSDASSERTevice.m
//  Favicinity
//
//  Created by Heiko Maaß on 28.05.10.
//  Copyright 2010 ODS Marketing. All rights reserved.
//

#import "UIDevice+ODSDevice.h"


@implementation UIDevice (ODSDevice)
- (BOOL) isIpodTouch {

	NSRange range = [self.model rangeOfString: @"iPod"];
	if (range.location != NSNotFound) {
		return YES;
	}
	else {
		return NO;
	}
}

- (BOOL) isIpad {
	NSRange range = [self.model rangeOfString: @"iPad"];
	if (range.location != NSNotFound) {
		return YES;
	}
	else {
		return NO;
	}
}
@end
