//
//  GSAdServerBannerView.m
//  ODSCommon
//
//  Created by Sandra Majewski on 16.10.13.
//  Copyright (c) 2013 online directory service gmbh. All rights reserved.
//

#import "GSAdServerBannerView.h"
#import "GSParameterButton.h"
#import "UIImage+ODSResizing.h"

@implementation GSAdServerBannerView {
    __strong GSParameterButton *goToURLButton;
}
@synthesize bannerClickURL;
@synthesize bannerTrackURL;
@synthesize bannerImage;
@synthesize adServerBannerViewDelegate;

- (void) drawRect:(CGRect)rect {
    [bannerImage drawInRect:self.frame];
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(c, 1.0);
    CGContextSetStrokeColorWithColor(c, [UIColor blackColor].CGColor);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 0,0);
    CGPathAddLineToPoint(path, NULL, rect.size.width, 0);
    CGPathAddLineToPoint(path, NULL, rect.size.width, rect.size.height);
    CGPathAddLineToPoint(path, NULL, 0, rect.size.height);
    CGPathAddLineToPoint(path, NULL, 0, 0);
    CGPathCloseSubpath(path);
    
    CGContextAddPath(c,path);
    CGContextStrokePath(c);
}

- (void)loadImage:(NSURL *)imageURL
{
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(requestRemoteImage:)
                                        object:imageURL];
    [queue addOperation:operation];
}

- (void)requestRemoteImage:(NSURL *)imageURL
{
    NSData *imageData = [[NSData alloc] initWithContentsOfURL:imageURL];
    UIImage *image = [[UIImage alloc] initWithData:imageData];
    
    [self performSelectorOnMainThread:@selector(placeImageInUI:) withObject:image waitUntilDone:YES];
}

- (void)placeImageInUI:(UIImage *)image
{
    bannerImage = image;
    CGSize imageSize = bannerImage.size;
    imageSize.width = self.superview.frame.size.width;

    bannerImage = [bannerImage imageScaledToMaxSize:imageSize];    

    [self setNeedsLayout];
    [adServerBannerViewDelegate dataDidLoadinBannerView:self];
}


- (void) layoutSubviews {
    CGRect contentFrame = CGRectMake(0,0,self.superview.frame.size.width,self.superview.frame.size.height);
    contentFrame.size.height = bannerImage.size.height;
    
    self.frame = contentFrame;
    goToURLButton.frame = contentFrame;
    goToURLButton.params = [NSMutableDictionary dictionaryWithObject:bannerClickURL forKey:@"bannerClickURL"];
    [self setNeedsDisplay];
}


#pragma mark - lifecycle

- (id)initWithFrame:(CGRect)frame 
{
    self = [super initWithFrame:frame];
    if (self) {
        goToURLButton = [[GSParameterButton alloc] initWithFrame:CGRectZero];
        [goToURLButton setBackgroundColor:[UIColor clearColor]];
        [goToURLButton addTarget:self.adServerBannerViewDelegate action:NSSelectorFromString(@"didTapAdServerBanner:") forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:goToURLButton];
    }
    return self;
}



@end
