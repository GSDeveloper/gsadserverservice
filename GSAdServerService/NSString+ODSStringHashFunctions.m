//
//  NSString+ODSStringHashFunctions.m
//  ODSCommon
//
//  Created by Heiko Maaß on 08.09.11.
//  Copyright (c) 2011 online directory service gmbh. All rights reserved.
//

#import "NSString+ODSStringHashFunctions.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (ODSStringHashFunctions)


// return a new autoreleased UUID string
+ (NSString *) uuid {
    // create a new UUID which you own
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    
    // create a new CFStringRef (toll-free bridged to NSString)
    // that you own
    NSString *uuidString = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
    
    // transfer ownership of the string
    // to the autorelease pool
    
    // release the UUID
    CFRelease(uuid);
    return uuidString;
}


@end

@implementation NSODSStringHashFunctionsDummy 
@end