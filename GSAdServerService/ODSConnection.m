//
//  GSXdatConnector.m
//  NotfallApp
//
//  Created by Sebastian Stenzel on 13.04.10.
//  Copyright 2010 Online Directory Marketing. All rights reserved.
//

#import "ODSConnection.h"
#import "Reachability.h"

@implementation ODSConnection
@synthesize connection, buffer, delegate, hostReachability;

- (void) loadRequest: (NSURLRequest *) urlRequest {
    
   NSLog(@"<NSURLRequest %@>", [[urlRequest URL] absoluteString]);
    
	if ([NSURLConnection canHandleRequest: urlRequest]) {
		[[NSNotificationCenter defaultCenter] addObserver: self
		 selector: @selector(reachabilityChanged:)
		 name: kReachabilityChangedNotification
		 object: nil];
		[self setHostReachability:[Reachability reachabilityWithHostName:[urlRequest.URL host]]];
		[self.hostReachability startNotifier];
		[self setConnection:[NSURLConnection connectionWithRequest: urlRequest delegate: self]];
	}
	else {
		NSError *error = [NSError errorWithDomain:@"AdServerService" code:101 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Request ist invalide", @"Reason", nil]];
		[delegate connectionDidFailWithError: error];
	}
}

- (void) cancelRequest {
	[[NSNotificationCenter defaultCenter] removeObserver: self];
	[connection cancel];
}

#pragma mark -
#pragma mark network reachability

- (void) reachabilityChanged: (NSNotification *) note {
	Reachability *curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: Reachability.class]);
	NetworkStatus reachabilityStatus = [curReach currentReachabilityStatus];
	if (reachabilityStatus == NotReachable) {
		NSError *error = [NSError errorWithDomain:@"AdServerService" code:199 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Verbindung zu Netz ist nicht vorhanden", @"Reason", nil]];
		[self cancelRequest];
		[self connection: connection didFailWithError: error];
	}
}

#pragma mark -
#pragma mark UrlConnectionDelegate

- (void) connection: (NSURLConnection *) c didReceiveResponse: (NSURLResponse *) response {
    
    //Für Header-Informationen im Debug-Fall
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
        NSDictionary *dictionary = [httpResponse allHeaderFields];
        NSLog(@"HTTP Response HeaderFields: %@",[dictionary description]);
    }
    
	if ([response respondsToSelector: @selector(statusCode)]) {
		long statusCode = [( (NSHTTPURLResponse *)response )statusCode];
		if (statusCode >= 400) {
			[c cancel];  // stop connecting; no more delegate messages
            NSError *statusError = [NSError errorWithDomain:@"AdServerService" code:140 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"HTTP-Status ist invalid", @"Reason", nil]];
			[self connection: c didFailWithError: statusError];
		}
	}

	if ([response expectedContentLength] == NSURLResponseUnknownLength) {
		[self setBuffer:[NSMutableData data]];
     
	}
	else {
        NSUInteger expectedSize = 0;
        if (response.expectedContentLength) {
            expectedSize = [NSNumber numberWithLongLong: response.expectedContentLength].unsignedIntValue;
        }
        
		[self setBuffer:[NSMutableData dataWithCapacity:expectedSize]];
	}
}

- (void) connection: (NSURLConnection *) connection didReceiveData: (NSData *) data {
    //NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //OLOG(@"ResponseData %@", responseString);
    
	[buffer appendData: data];
}

- (void) connectionDidFinishLoading: (NSURLConnection *) connection {
	[[NSNotificationCenter defaultCenter] removeObserver: self];
	[delegate connectionDidLoadData:[NSData dataWithData: buffer]];

}

- (void) connection: (NSURLConnection *) connection didFailWithError: (NSError *) error {
	[[NSNotificationCenter defaultCenter] removeObserver: self];
	[delegate connectionDidFailWithError: error];
}

#pragma mark -
#pragma mark life cycle

- (void) dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver: self];
	[self setDelegate: nil];
}

@end